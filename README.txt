
Profile tokens

The module provides tokens for user object with profile fields

To install, place the entire module folder into your modules directory.
Go to Administer -> Site Building -> Modules and enable the Profile Tokens module.
